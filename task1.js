// setup requirements
const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputFile = "input2.json";
const outputFile = "output2.json";

var result = {}
console.log("loading input file", inputFile);
jsonfile.readFile(inputFile, function(err, body){
    let names = body.names;
    result.email = names.map(name => {
        // this get 5 random characters and add @gmail.com
        let endstring = randomstring.generate(5).concat("@gmail.com");
        // this reversed the string and add them with the previous string
        // could be combined into one line, but I don't want it to be too condensed
        return name.split("").reverse().join("").concat(endstring);
    });
    console.log(result);
    jsonfile.writeFile(outputFile, result, {spaces: 2}, function(err) {
        console.log("All done!");
      });
});